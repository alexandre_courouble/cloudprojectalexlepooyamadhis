# Virtualization Using Two Major Cloud Providers: Amazon EC2 and Microsoft Azure #

### What is this repository for? ###

Benchmark Amazon and Azure cloud services in different aspects in order to compare the performance between the two cloud service provider. Our results can help a Canadian company, Alpha Soft, selecte its suitable cloud service provider.

###File description###
- **bash** folder: includes bash scripts to benchmark the six types of AWS and Azure instances, as well as the consecutive benchmarking script for AWS t2.micro's CPU.
- **python** folder: includes the Python script to calculate the start-up time of Amazon instances. This folder also contains the Python benchmarking scripts, which were not eventually used due to the incomplete output data.  

### How should I use the benchmarking scripts? ###

We use different scripts to test differnt types of virtual machine. Users can just run ```./autorun``` for each type of instances.
For example, to benchmark AWS t2.micro, users can do the following operations:
```bash
cd analytic_scripts/bash/aws_micro
./autorun
```


### Contact ###

le.an@polymtl.ca  
alexandre.courouble@polymtl.ca  
pooya.musavi@polymtl.ca  
mahdis.zolfagharinia@polymtl.ca