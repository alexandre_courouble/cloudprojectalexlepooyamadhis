from timeit import default_timer
import boto3

ec2 = boto3.resource("ec2")

amazon_t2micro_instanceId  = 'i-a834d770'
amazon_t2medium_instanceId = 'i-72036ea8'
amazon_c3large_instanceId  = 'i-1b6a9ec3'

def getState(instance):
	inst = ec2.Instance(id=instance)
	state = inst.state["Name"]
	return state
	
def startInstance(instance):
	start_time = default_timer()
	inst = ec2.Instance(id=instance)
	inst.start()
	while True:
		if('running'==getState(instance).strip().lower()):
			break		
	duration=default_timer()-start_time
	print 'instance started and it took: {0} '.format(duration)
	
def stopInstance(instance):
	start_time = default_timer()
	inst = ec2.Instance(id=instance)
	inst.stop()
	while True:
		if('stopped'==getState(instance).strip().lower()):
			break		
	duration=default_timer()-start_time
	print 'instance stopped and it took: {0}'.format(duration)

def testMicro():
	state=getState(amazon_t2micro_instanceId)
	if state != 'running':
		print "starting a stopped Micro instance ...."
		startInstance(amazon_t2micro_instanceId)
		print "Stopping a running Micro instance ...."
		stopInstance(amazon_t2micro_instanceId)
	else:
		print "Stopping a running Micro instance ...."
		stopInstance(amazon_t2micro_instanceId)
		print "starting a stopped Micro instance ...."
		startInstance(amazon_t2micro_instanceId)

def testMedium():
	state=getState(amazon_t2medium_instanceId)
	if state != 'running':
		print "starting a stopped Medium instance ...."
		startInstance(amazon_t2medium_instanceId)
		print "Stopping a running Medium instance ...."
		stopInstance(amazon_t2medium_instanceId)
	else:
		print "Stopping a running Medium instance ...."
		stopInstance(amazon_t2medium_instanceId)
		print "starting a stopped Medium instance ...."
		startInstance(amazon_t2medium_instanceId)

def testLarge():
	state=getState(amazon_c3large_instanceId)
	if state != 'running':
		print "starting a stopped Large instance ...."
		startInstance(amazon_c3large_instanceId)
		print "Stopping a running Large instance ...."
		stopInstance(amazon_c3large_instanceId)
	else:
		print "Stopping a running Large instance ...."
		stopInstance(amazon_c3large_instanceId)
		print "starting a stopped Large instance ...."
		startInstance(amazon_c3large_instanceId)
		
def main():
	testMicro()
	testMedium()
	testLarge()
			
if __name__=="__main__":
	main()