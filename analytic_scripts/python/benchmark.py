import subprocess, csv

# Execute a shell command
def shellCommand(command_str):
    cmd = subprocess.Popen(command_str, shell=True, stdout=subprocess.PIPE)
    cmd_out, cmd_err = cmd.communicate()
    return cmd_out

# Test one benchmark for 5 times
def testBenchmark(command_str, name):
    print 'Testing', name, '...'
    output_list = list()
    for i in range(5):
        print 'Round', i+1
        res = shellCommand(command_str)
        output_list.append('\nRound %d:\n\n%s' %(i+1,res))
    # concatenate results for output
    output_str = ('\n'+'-'*50+'\n').join(output_list)
    # output results into a file
    with open('%s/%s.txt' %(output_dir,name.lower()), 'w') as f:
        f.write(output_str)
    return

if __name__ == '__main__':
    # create output file
    output_dir = 'benchmark_output'
    subprocess.call(['mkdir', '-p', output_dir])
    # Load values    
    with open('params.config', 'r') as f:
        csvreader = csv.reader(f, delimiter=':')
        for line in csvreader:
            benchmark = line[0]
            command_str = line[1].strip()
            testBenchmark(command_str, benchmark)
    
    print '\nDone.'